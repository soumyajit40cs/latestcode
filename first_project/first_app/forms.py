from django import forms
from django.core import validators
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from first_app.models import Contacts, Userprofileinfo
from django.forms import ModelForm


class UserForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
       model = User
       fields = ['username','email','password']

class UserprofileinfoForm(ModelForm):
    class Meta:
        model = Userprofileinfo
        fields = ['pro_url','pro_pic']



def name_val(value):
    if value[0].lower() != 'z':
        raise forms.ValidationError('name start with z')

class FormsName(forms.Form):
    name = forms.CharField(validators=[name_val])
    named = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    comment = forms.CharField(widget=forms.Textarea, required=False)
    hidfld = forms.CharField(widget=forms.HiddenInput, required=False, initial='123')


def clean_named(self):
    user_name = self.cleaned_data['named']
    if user_name is None:
        raise forms.ValidationError("Give me a username", code="named",)
        return  user_name

def clean(self):
        cleaned_data = super().clean()
        cc_myself = cleaned_data("name")
        subject = cleaned_data("name2")
        raise forms.ValidationError('fck')


class ContactForm(ModelForm):
    class Meta:
       model = Contacts
       fields = ['name','email','msg']
       labels = {
            "name": "Your Name",
            "msg": "Your comment"
        }
