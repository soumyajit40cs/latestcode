from django import template

register = template.Library()

@register.filter(name='basic_filter')

def basic_filter(value,args):
    return value.replace(args, '&')
    #return value
