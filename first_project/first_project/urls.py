"""first_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.conf.urls import url
from django.conf.urls import *
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from first_app import views
from django.conf import settings
from django.conf.urls.static import static
#from django.contrib.auth.views import login, logout

urlpatterns = [
    path('', views.index, name='index'),
    path('blog/', include('first_app.urls')),
    #path('help/', include('first_app.urls')),
    path('help/', views.help, name='help'),
    path('customform/', views.customform, name='customform'),
    path('contact/', views.conform, name='contactForm'),
    path('registration/', views.registration, name='registration'),
    #path('user_login/', views.user_login, name='login'),
    #path('user_logout/', views.user_logout, name='logout'),
    path('admin/', admin.site.urls,name='adminu'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('cbt/',views.CBTview.as_view()),
    path('tempview/',views.Templceviewss.as_view()),
    path('topiclist/',views.TopicinfoList.as_view(),name='topiclist'),
    path('detailsinfo/<int:pk>/', views.TopicdetailsinfoList.as_view(), name='detail'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Demo Admin"
admin.site.site_title = "Demo Admin Portal"
admin.site.index_title = "Welcome to Python POC Portal"
