import os

import random
os.environ.setdefault('DJANGO_SETTINGS_MODULE','first_project.settings')
import django
django.setup()

from faker import Faker

from first_app.models import Topic, Webpage, AccessRecord

fakergan = Faker()

topics = ['aaa','bbb','ccc','ddd']

def add_topic():
    t = Topic.objects.get_or_create(top_name=random.choice(topics))[0]
    t.save
    return t

def populate(N=5):

    for entry in range(N):

            topic_data = add_topic()
            fake_name = fakergan.company()
            fake_url =  fakergan.url()
            fake_date = fakergan.date()

            webpg = Webpage.objects.get_or_create(topic=topic_data, name=fake_name, url=fake_url)[0]
            accrd = AccessRecord.objects.get_or_create(name=webpg, date=fake_date)[0]

if __name__ == '__main__':
    print('populating script')

populate(20)
print('dummy data genaration complete')
